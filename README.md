# nitrous-renderbuffer #

A variation of the ThreeJS EffectComposer based off of [Cabibo's PhysicsRenderer](https://github.com/cabbibo/PhysicsRenderer) written using ES6 style Javascript.

### How do I get set up? ###

* Pull down repo
* RenderBuffer.js contains the ES6 source. [BabelJS](https://babeljs.io) is included in the package.json file, so to mess around with things, run `npm install` first before making any changes otherwise you'll get errors in the output. [For instruction on how to use the BabelJS cli, see here](https://babeljs.io/docs/usage/cli/)
* You can of course, optionally use your own preferred transpiler.
* Run `npm start` to compile the source which will run off of the `index.js` file. The compiled file will appear in public/js/Renderbuffer.js
