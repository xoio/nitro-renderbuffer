/**
 * Simple mostly static class to check for basic capabilities
 */
var defaultErrorString = "Oh NOEs! your browser doesn't appear to support WebGL";

var gldefaultOpts = {
    errorString:"Oh NOEs! your browser doesn't appear to support WebGL",
    saveContext:true
};

class Capabilities {


    /**
     * Does a basic check for mobile based on the UserAgent string
     */
    static isMobile(){
        let ua = navigator.userAgent;
        var search = new RegExp("Android|webOS|iPhone|iPad|iPod|BlackBerry");

        if(ua.search(search) !== -1){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Checks for the existance of Three.js
     * @returns {boolean}
     */
    static haveThreejs(){
        if(window.THREE !== undefined){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Simple function to check for WebGL. Will also simultaneously return canvas and context or a
     * THREE.WebGLRenderer when using Three.js. Will check for WebGL2 first before going to WebGL1
     * @param opts object of options. See {@link gldefaultOpts} for whats available
     * @returns {*}
     */
    static canWebGL(opts=gldefaultOpts){
        let canvas = document.createElement("canvas");
        let ctx = null;
        let types = [
            "webgl2",
            "experimental-webgl2",
            "webgl",
            "experimental-webgl"
        ];

        for(var i = 0; i < 4; ++i){
            let _ctx = canvas.getContext(types[i]);
            if(_ctx !== null){
                ctx = _ctx;
            }
        }

        //if context is null, console error message
        if(ctx === null){
            console.error(opts.errormessage);
            return false;
        }else{
            //otherwise, package result
            var result = {};

            try {

                if(opts.saveContext){
                    result = new THREE.WebGLRenderer();
                    window.globalContext = result.getContext();
                }else{
                    result = true;
                }


            }catch(e){
                if(opts.saveContext){
                    result = {
                        canvas:canvas,
                        gl:ctx
                    }
                    window.globalContext = result.ctx;

                }else{
                    result = true;
                }

            }

            return result;
        }
    }
}

export default Capabilities;
